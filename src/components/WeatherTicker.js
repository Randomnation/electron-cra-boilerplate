import React, { Component } from 'react';
import axios from "axios";
import { config } from '../utils/config';
import '../Main.css';

export default class WeatherTicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weather: {},
      speed: 2
    };

    this.getWeather = this.getWeather.bind(this);
  }

  componentDidMount() {
    this.getWeather();
  }

  getWeather = async () => {
    let reqConfig = {
      headers: {
        Accept: "application/json"
      }
    }
    let res = await axios.get(`http://api.weatherunlocked.com/api/current/-31.642,115.706?app_id=${config.APPID}&app_key=${config.APIKEY}`, reqConfig);
    let data = res.data;
    this.setState({ weather: data })
    console.log(this.state.weather)
  };

  render() {
    const weather = this.state.weather;
    return (
      <div className="ticker">
        Current Temperature: {weather.temp_c}&deg;C
      </div>
    );
  };
}