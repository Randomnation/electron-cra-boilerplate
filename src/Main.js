import React, { Component } from 'react';
import WeatherTicker from './components/WeatherTicker';
import './Main.css';


export default class Main extends Component {
  render() {
    return (
      <div>
        <div className="App">
          <header className="App-header" />
        </div>
        <div className="container">
          <div className="row">
            <div className="col-sm-3">
             <WeatherTicker />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
